<?php
class DBConfig{
    private PDO $pdo;

    public function __construct(){
        $params = [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_PERSISTENT => true,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ
        ];

        $this->pdo = new PDO("mysql:host=localhost;dbname=gnotes", 'gnotes', 'gnotes', $params);
    }

    public function getPdo(): PDO{
        return $this->pdo;
    }
}